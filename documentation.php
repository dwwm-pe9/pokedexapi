<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Documentation des routes</title>
    <!-- Ajout de Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            padding: 20px;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1>Documentation des routes</h1>
        <ul class="list-group">
            <li class="list-group-item">
                <strong>GET /index.php?action=get_pokemons</strong>: Récupère la liste de tous les Pokémon.
            </li>
            <li class="list-group-item">
                <strong>GET /index.php?action=get_pokemon&id={id}</strong>: Récupère les informations d'un Pokémon
                spécifique en fonction de son ID.
            </li>
            <li class="list-group-item">
                <strong>POST /index.php?action=add_pokemon</strong>: Ajoute un nouveau Pokémon à la base de données.
            </li>
            <li class="list-group-item">
                <strong>PUT /index.php?action=update_pokemon&id={id}</strong>: Met à jour les informations d'un Pokémon
                existant en fonction de son ID.
            </li>
            <li class="list-group-item">
                <strong>DELETE /index.php?action=delete_pokemon&id={id}</strong>: Supprime un Pokémon de la base de
                données en fonction de son ID.
            </li>
        </ul>

        <h2 class="mt-4">Paramètres d'action et d'ID :</h2>
        <p>Les paramètres d'action et d'ID sont utilisés dans l'URL pour spécifier l'action à effectuer et l'identifiant
            du Pokémon concerné pour le GET, EDIT, DELETE.</p>

        <h3 class="mt-3">Exemples :</h3>
        <ul>
            <li>Exemple d'URL pour récupérer tous les Pokémon : <code>/index.php?action=get_pokemons</code></li>
            <li>Exemple d'URL pour récupérer un Pokémon spécifique par ID :
                <code>/index.php?action=get_pokemon&id=1</code></li>
            <li>Exemple d'URL pour ajouter un nouveau Pokémon : <code>/index.php?action=add_pokemon</code></li>
            <li>Exemple d'URL pour mettre à jour un Pokémon existant par ID : <code>/index.php?action=update_pokemon
                </code>
            </li>
        </ul>
    </div>
</body>