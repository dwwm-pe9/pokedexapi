<?php

$routes = [
    'get_all_pokemons',
    'create_pokemon',
    'get_pokemon',
    'update_pokemon',
    'delete_pokemon'
];

require_once("utils/routes.php");
//Si il n'y a pas d'action ou si elle n'est pas dans la liste
if (!isset($_GET["action"]) || !in_array($_GET["action"],$routes)) {    
    documentation();
    exit();
}

//Si je ne suis pas dans la documentation, j'enverrais du json
//header('Content-Type: application/json');
$getAction = $_GET['action'];
//Le parametre ID n'est pas nécessaire à chaque requete, il sera donc parfois null
$getId = isset($_GET['id']) ? $_GET['id'] : null;

// Gérer les différentes actions avec match
match ($getAction) {
    'get_all_pokemons' => getAllPokemons(),
    'create_pokemon' => createPokemon(),
    'get_pokemon' => getPokemonById($getId),
    'update_pokemon' => editPokemon($getId),
    'delete_pokemon' => deletePokemon($getId),
    default => documentation()
};
