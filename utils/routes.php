<?php

require_once("utils/databaseManager.php");

function documentation()
{
    include ("documentation.php");
    exit();
}


function getAllPokemons()
{
    $pdo = connectDB();


    $reponse = $pdo->query('SELECT * FROM pokemon');
    // Plusieurs resultat fetchAll
    $data = $reponse->fetchAll();

    echo json_encode($data);

    exit();
}

function createPokemon()
{
    // Récupérer les données du nouveau Pokémon depuis le corps de la requête
    $data = json_decode(file_get_contents('php://input'), true);

    if (verifPokemonData($data)) {
        try {
            $pdo = connectDB();
            // Préparer et exécuter la requête pour ajouter un nouveau Pokémon dans la base de données
            $stmt = $pdo->prepare("INSERT INTO Pokemon (generation_number, category, nameFr, nameJP, image, imageShiny, height, weight, catchRate)
                                        VALUES (:generation_number, :category, :nameFr, :nameJP, :image, :imageShiny, :height, :weight, :catchRate)");
            $stmt->execute([
                ':generation_number' => $data['generation_number'],
                ':category' => $data['category'],
                ':nameFr' => $data['nameFr'],
                ':nameJP' => $data['nameJP'],
                ':image' => $data['image'],
                ':imageShiny' => $data['imageShiny'],
                ':height' => $data['height'],
                ':weight' => $data['weight'],
                ':catchRate' => $data['catchRate']
            ]);

            echo (true); // Succès de l'opération d'ajout
        } catch (PDOException $e) {
            // Gérer l'erreur ici (par exemple, en enregistrant l'erreur dans un journal)
            echo (json_encode($e));
        }
    }
}

function editPokemon($id)
{
    $pdo = connectDB();

    $data = json_decode(file_get_contents('php://input'), true);

    if (verifPokemonData($data)) {

        try {
            $stmt = $pdo->prepare(
                "UPDATE Pokemon SET 
                generation_number = :generation_number,
                category = :category,
                nameFr = :nameFr,
                nameJP = :nameJP,
                image = :image,
                imageShiny = :imageShiny,
                height = :height,
                weight = :weight,
                catchRate = :catchRate 
                WHERE id = :id"
            );
            $stmt->execute([
                ':generation_number' => $data['generation_number'],
                ':category' => $data['category'],
                ':nameFr' => $data['nameFr'],
                ':nameJP' => $data['nameJP'],
                ':image' => $data['image'],
                ':imageShiny' => $data['imageShiny'],
                ':height' => $data['height'],
                ':weight' => $data['weight'],
                ':catchRate' => $data['catchRate'],
                ':id' => $id
            ]);

            return true;
        } catch (PDOException $e) {
            echo (json_encode($e->getMessage()));
        }
    }
}

function getPokemonById($id)
{
    $pdo = connectDB();
    echo (json_encode(findPokemonById($pdo,$id) ?? "Aucun résultat"));

}

function deletePokemon($id)
{
    $pdo = connectDB();

    try {
        $stmt = $pdo->prepare(
            "DELETE FROM Pokemon WHERE id = :id"
        );
        $stmt->execute([
            ':id' => $id
        ]);

        return true;
    } catch (PDOException $e) {
        echo (json_encode($e->getMessage()));
    }
}



function verifPokemonData($data)
{

    $expected_types = [
        'generation_number' => 'integer',
        'category' => 'string',
        'nameFr' => 'string',
        'nameJP' => 'string',
        'image' => 'string',
        'imageShiny' => 'string',
        'height' => 'double',
        'weight' => 'double',
        'catchRate' => 'double'
    ];

    foreach ($expected_types as $param => $expected_type) {

        if (!isset($data[$param])) {
            echo json_encode(["error" => "Le paramètre $param est manquant."]);
            exit();
        } elseif (gettype($data[$param]) !== $expected_type) {
            echo json_encode(["error" => "Le paramètre $param doit etre de type $expected_type."]);
            exit();
        }
    }
    return true;
}


?>