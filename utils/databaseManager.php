<?php

function connectDB(): PDO
{

    try {

        $host = "localhost";
        $databaseName = "pokedexpe9";
        $user = "root";
        $password = "";

        $pdo = new PDO("mysql:host=" . $host . ";port=3306;dbname=" . $databaseName . ";charset=utf8", $user, $password);

        configPdo($pdo);

        return $pdo;
    } catch (Exception $e) {

        //Lancer l'erreur
        //throw $e;

        echo ("Erreur à la connexion: " .  $e->getMessage());

        exit();
    }
}

function configPdo(PDO $pdo): void
{
    // Recevoir les erreurs PDO ( coté SQL )
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // Choisir les indices dans les fetchs
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}

function findPokemonById(PDO $pdo, int $id): array
{
    $query = $pdo->prepare('SELECT * FROM pokemon WHERE id = :id');
    $query->execute([
        ":id" => $id
    ]);
    // Un seul resultat attendu fetch
    return $query->fetch();
}

function findPokemonByName(PDO $pdo, string $name): array
{
    $query = $pdo->prepare('SELECT * FROM pokemon WHERE name = :name');
    $query->execute([
        ":name" => $name
    ]);
    return $query->fetchAll();
}
